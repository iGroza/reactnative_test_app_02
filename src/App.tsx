import { StatusBar } from 'expo-status-bar';
import { observer, Provider } from 'mobx-react';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import useCachedResources from './hooks/useCachedResources';
import { Navigation } from './navigation';

const stores = {}

export const App = observer(function () {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return <></>;
  } else {
    return (
      <Provider {...stores}>
        <SafeAreaProvider>
          <StatusBar />
          <Navigation />
        </SafeAreaProvider>
      </Provider>
    );
  }
})
