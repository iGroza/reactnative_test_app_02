import { Http } from "../http";
import { IResponseExchangeList } from "./GetExchangeList.types";

export class GetExchangeList {
    private http: Http = new Http()
    private abortController = new AbortController()

    async get() {
        return await this.http.GET<IResponseExchangeList>("/public?command=returnTicker", { signal: this.abortController.signal });
    }

    abort() {
        this.abortController.abort()
    }
}