export interface IResponseExchangeItem {
    id?: number;
    last?: string;
    lowestAsk?: string;
    highestBid?: string;
    percentChange?: string;
    baseVolume?: string;
    quoteVolume?: string;
    isFrozen?: string;
    high24hr?: string;
    low24hr?: string;
}

export interface IResponseExchangeList {
    [key: string]: IResponseExchangeItem;
}

export interface IErrorResponseExchangeItem {
    message: string;
    description: string;
}

export interface IErrorResponseExchangeList {
    [key: string]: IErrorResponseExchangeItem;
}
