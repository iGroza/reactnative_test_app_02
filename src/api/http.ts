export class Http {
    private readonly BASE_URL = "https://poloniex.com";

    public GET<T>(path: string, options: RequestInit): Promise<T> {
        return fetch(this.BASE_URL + path, options)
            .then(data => data.json())
    }
}