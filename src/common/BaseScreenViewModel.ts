export interface BaseScreenViewModel {
    readonly isActive: boolean;

    onActive(): void;
    onDestroy(): void;
}