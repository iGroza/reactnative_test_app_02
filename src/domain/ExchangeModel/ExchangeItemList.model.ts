import { IErrorResponseExchangeList, IResponseExchangeList } from "../../api/Exchange";
import { IExchangeItem } from "./ExchangeItem.types";

export class ExchangeItemListModel {

    public list: IExchangeItem[];

    constructor(items: IResponseExchangeList | IErrorResponseExchangeList ) {
        this.list = Object.entries(items).map(item => ({ name: item[0], ...item[1] }))
    }
}