export interface ITableProps<T> {
    data?: T[];
    sortByFieldName?(a: string, b: string): number;
    keyExtractor(item: T): string | number | undefined;

    header: {
        // data: string[];
        renderItem: (props: IHeaderRenderItemProps) => JSX.Element | null;
    };

    body: {
        // data: T[];
        renderItem: (props: IBodyRenderItemProps<T>) => JSX.Element | null;
    };
}

interface IHeaderRenderItemProps {
    item: string;
    index: number;
}

interface IBodyRenderItemProps<T> {
    item: T;
    fieldkey: keyof T;
    verticalIndex: number;
    horizontalIndex: number;
}

