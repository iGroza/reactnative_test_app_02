import { observer } from 'mobx-react';
import React from 'react';
import { Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { ITableProps } from './Table.types';

@observer
export class Table<T> extends React.Component<ITableProps<T>> {
    render() {
        const { body, header, data, sortByFieldName,keyExtractor } = this.props;

        if (!data || (!!data && !data.length)) {
            return <Text> Empty data </Text>
        }

        const headers = Object.keys(data[0]).sort(sortByFieldName);

        return (
            <ScrollView showsVerticalScrollIndicator={true}>
                <ScrollView horizontal={true} showsVerticalScrollIndicator={true}>
                    <View style={{ display: "flex", flexDirection: "column" }}>
                        <View style={{ display: "flex", flexDirection: "row" }}>
                            {headers.map((item, horizontalIndex) => (
                                <View style={{ display: "flex", flexDirection: "column" }} key={horizontalIndex}>
                                    {header.renderItem({ item, index: horizontalIndex })}
                                    {data.map((i, verticalIndex) => (
                                        <React.Fragment key={keyExtractor(data[verticalIndex])}>
                                            {body.renderItem({ item: data[verticalIndex], verticalIndex, horizontalIndex, fieldkey: item as keyof T })}
                                        </React.Fragment>
                                    )
                                    )}
                                </View>
                            ))}
                        </View>
                    </View>
                </ScrollView>
            </ScrollView>
        )
    }
}