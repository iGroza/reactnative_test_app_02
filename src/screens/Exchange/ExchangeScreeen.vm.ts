import { action, computed, makeObservable, observable } from "mobx";
import { GetExchangeList } from "../../api/Exchange";
import { BaseScreenViewModel } from "../../common/BaseScreenViewModel";
import { IExchangeItem } from "../../domain/ExchangeModel";
import { ExchangeItemListModel } from "../../domain/ExchangeModel/ExchangeItemList.model";

export class ExchangeSreenVM implements BaseScreenViewModel {

    @observable
    public isActive: boolean = false;

    @observable
    public isError: boolean = false;

    @observable
    private timer?: NodeJS.Timeout;
    private UPDATE_INTERVAL = 5000;

    @observable
    private _exchangeListModel?: ExchangeItemListModel;
    private _getExchangeList: GetExchangeList;

    constructor() {
        makeObservable(this);
        this._getExchangeList = new GetExchangeList()
    }

    @computed get exchangeList() {
        return this._exchangeListModel?.list
    }

    @computed get sortByFieldName() {
        return (a: string, b: string) => a === "id" ? -1 : 0;
    }

    keyExtractor(item: IExchangeItem) {
        return item?.id;
    }

    @action
    async onActive() {
        try {
            // throw new Error("simulated error")
            const response = await this._getExchangeList.get();
            this._exchangeListModel = new ExchangeItemListModel(response);
            this.isActive = true;
            this.isError = false;
        } catch (e) {
            const err: Error = e;
            this._exchangeListModel = new ExchangeItemListModel({
                [err.name]: {
                    message: "ошибка",
                    description: err.message
                }
            });

            this.isError = true;
            this.isActive = true;
        }

        if (!this.timer) {
            this.timer = setInterval(action(() => {
                this.onActive();
            }), this.UPDATE_INTERVAL)
        }
    }

    @action
    onDestroy(): void {
        if (!this.isActive) this._getExchangeList.abort();

        clearInterval(this.timer!);

        this.isActive = false;
        this.isError = false;
        this.timer = undefined;
        this._exchangeListModel = undefined;
    }

}