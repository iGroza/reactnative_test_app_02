import { StackNavigationScreenProps } from './../../navigation';

export type ExchangeScreenStackList = {
    Exchange: undefined;
}

export interface IExchangeScreenProps extends StackNavigationScreenProps {
}