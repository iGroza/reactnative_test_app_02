import { createStackNavigator } from '@react-navigation/stack';
import { observer } from 'mobx-react';
import * as React from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import { Table } from '../../components/Table';
import { ExchangeScreenStackList, IExchangeScreenProps } from './ExchangeScreen.types';
import { IExchangeItem } from "../../domain/ExchangeModel";
import { ExchangeSreenVM } from './ExchangeScreeen.vm';
import Colors from '../../constants/Colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
});

@observer
class ExchangeScreen extends React.PureComponent<IExchangeScreenProps> {

    private _vm = new ExchangeSreenVM();

    componentDidMount() {
        const { navigation } = this.props;
        const vm = this._vm;
        navigation.addListener("focus", function () { vm.onActive() });
        navigation.addListener("blur", function () { vm.onDestroy() });
    }

    componentWillUnmount() {
        const { navigation } = this.props;
        const vm = this._vm;
        navigation.removeListener("focus", function () { vm.onActive() });
        navigation.removeListener("blur", function () { vm.onDestroy() });
    }

    render() {
        if (!this._vm.isActive) {
            return (
                <View style={[styles.container, styles.horizontal]}>
                    <ActivityIndicator size="large" color={Colors["light"].tint} />
                </View>)
        }

        return (
            <View style={styles.container}>
                <Table<IExchangeItem>
                    data={this._vm.exchangeList}
                    sortByFieldName={this._vm.sortByFieldName}
                    keyExtractor={this._vm.keyExtractor}
                    header={{
                        renderItem: ({ item, index }) => (
                            <View
                                style={{
                                    padding: 5,
                                    backgroundColor: index % 2 === 0 ? "gray" : "white",
                                    height: 30,
                                    alignItems: "center"
                                }}>
                                <Text style={{
                                    color: index % 2 === 0 ? "white" : "black",
                                }}>
                                    {item}
                                </Text>
                            </View>)
                    }}
                    body={{
                        renderItem: ({ item, fieldkey, verticalIndex, horizontalIndex }) => (
                            <View
                                style={{
                                    padding: 5,
                                    backgroundColor: horizontalIndex % 2 === 0 ? "gray" : "white",
                                    height: 30,
                                    alignItems: "center"
                                }}>
                                <Text style={{
                                    color: horizontalIndex % 2 === 0 ? "white" : "black",
                                }}>
                                    {item[fieldkey]}
                                </Text>
                            </View>)
                    }}
                />
            </View>
        );
    }
}

const ExchangeScreenStack = createStackNavigator<ExchangeScreenStackList>();

export function ExchangeStackTab() {
    return (
        <ExchangeScreenStack.Navigator>
            <ExchangeScreenStack.Screen
                name="Exchange"
                component={ExchangeScreen}
                options={{ headerTitle: 'Exchange' }}
            />
        </ExchangeScreenStack.Navigator>
    );
}