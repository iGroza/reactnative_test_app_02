import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { observer } from 'mobx-react';
import { StyleSheet, Text, View } from 'react-native';
import { AboutScreenStackList } from './AboutScreen.types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});

@observer
class AboutScreen extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>developement by Kirill Ageychenko</Text>
                <Text style={styles.title}>v 1.0</Text>
            </View>
        );
    }
}

const AboutScreenStack = createStackNavigator<AboutScreenStackList>();

export function AboutStackTab() {
    return (
        <AboutScreenStack.Navigator>
            <AboutScreenStack.Screen
                name="About"
                component={AboutScreen}
                options={{ headerTitle: 'About' }}
            />
        </AboutScreenStack.Navigator>
    );
}