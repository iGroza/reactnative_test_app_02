import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { observer } from 'mobx-react';
import * as React from 'react';
import Colors from '../../constants/Colors';
import { AboutStackTab } from '../../screens/About';
import { ExchangeStackTab } from '../../screens/Exchange';
import { BottomTabList } from './BottomTabNavigator.types';

const BottomTab = createBottomTabNavigator<BottomTabList>();

export const BottomTabNavigator = () => {

  return (
    <BottomTab.Navigator
      initialRouteName="Exchange"
      tabBarOptions={{ activeTintColor: Colors['light'].tint }}
      backBehavior="none">

      <BottomTab.Screen
        name="Exchange"
        component={ExchangeStackTab}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="document-sharp" color={color} />,
        }}
      />

      <BottomTab.Screen
        name="About"
        component={AboutStackTab}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="information-circle-sharp" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

function TabBarIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}
