import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { GreetingScreen } from '../screens/Greeting/';
import { BottomTabNavigator } from './BottomTabNavigator/';
import { RootStackParamList } from './Navigation.types';
import { observer } from 'mobx-react';

export const Navigation = observer(() => {
    return (
        <NavigationContainer>
            <RootNavigator />
        </NavigationContainer>
    );
})

const Stack = createStackNavigator<RootStackParamList>();

const RootNavigator = observer(() => {
    return (
        <Stack.Navigator
            screenOptions={{ headerShown: false }}
            initialRouteName="Greeting">
            <Stack.Screen name="Main" component={BottomTabNavigator} />
            <Stack.Screen name="Greeting" component={GreetingScreen} options={{ title: 'Cloudfactory!' }} />
        </Stack.Navigator>
    );
})
