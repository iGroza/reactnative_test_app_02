import { StackScreenProps } from "@react-navigation/stack"

export type RootStackParamList = {
    Greeting: undefined;
    Main: undefined;
}

export type StackNavigationScreenProps = StackScreenProps<RootStackParamList>