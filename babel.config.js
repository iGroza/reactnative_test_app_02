module.exports = function (api) {
  api.cache(true);
  return {
    "presets": ["babel-preset-expo"],
    "sourceMaps": "inline",
    "plugins": [
      'transform-inline-functions',
      ['@babel/plugin-proposal-decorators', { 'legacy': true }],
      ["@babel/plugin-proposal-class-properties", { "loose": false }],
      '@babel/plugin-transform-runtime',
    ],
    "env": {
      "production": {
        "plugins": ["transform-remove-console"]
      }
    }
  };
};


